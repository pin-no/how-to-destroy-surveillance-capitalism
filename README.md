How to Destroy Surveillance Capitalism
======================================
by Cory Doctorow

Translations of the book initially published on
https://onezero.medium.com/how-to-destroy-surveillance-capitalism-8135e6744d59
and
https://craphound.com/category/destroy/ .

This translation effort is conducted with approval from Cory Doctorow.
The publisher, Petter Reinholdtsen, got approval from the author to
publish a translated Norwegian edition using the Creative Commons
Attribution-NoDerivatives 4.0 International (CC BY-ND 4.0) licence.
See https://creativecommons.org/licenses/by-nd/4.0/ for more
information on the license.

The result can be viewed on
https://pin-no.gitlab.io/how-to-destroy-surveillance-capitalism/
and http://www.hungry.com/~pere/publisher/ .

The latest files for this book are available from
https://gitlab.com/pin-no/how-to-destroy-surveillance-capitalism/ .

The translation is done using Weblate on
https://hosted.weblate.org/projects/rms-personal-data-safe/how-to-destroy-surveillance-capitalism/ .
