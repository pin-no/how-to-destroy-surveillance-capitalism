<?xml version='1.0' encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version='1.0'>

  <!-- hack to add [hyphens] option to url package -->
  <xsl:param name="latex.class.book">book}
\usepackage[hyphens]{url</xsl:param>
  <xsl:param name="latex.class.article">article}
\usepackage[hyphens]{url</xsl:param>

  <xsl:param name="latex.class.options">a4paper,openright,twoside</xsl:param>
  <xsl:param name="page.width">6in</xsl:param>
  <xsl:param name="page.height">9in</xsl:param>
  <xsl:param name="page.margin.inner">0.8in</xsl:param>
  <xsl:param name="page.margin.outer">0.55in</xsl:param>
  <xsl:param name="page.margin.top">0.55in</xsl:param>
  <xsl:param name="page.margin.bottom">0.55in</xsl:param>
  <xsl:param name="ulink.show">1</xsl:param>
  <xsl:param name="ulink.footnotes">1</xsl:param>
  <xsl:param name="double.sided">1</xsl:param>
  <xsl:param name="doc.collab.show">0</xsl:param>
  <xsl:param name="latex.output.revhistory">0</xsl:param>
  <xsl:param name="doc.publisher.show">0</xsl:param>
  <xsl:param name="draft.mode">no</xsl:param>
  <xsl:param name="draft.watermark">1</xsl:param>
  <xsl:param name="doc.section.depth">0</xsl:param>

  <!-- The TOC links in the titles, and in blue. -->
  <!-- ensure URLs in the text do not end up light gray too -->
  <xsl:param name="latex.hyperparam">linktocpage,colorlinks,linkcolor=black,urlcolor=black,pdfstartview=FitH</xsl:param>

  <!-- insert a few latex tricks at the top of the .tex document -->
  <xsl:param name="latex.begindocument">
    <xsl:text>% start of latex.begindocument

% Trick to avoid many words sticking out of the right margin of the text.
\sloppy

% The microtype package provides the ability to micromanage your
% typography. When invoked without any options it does some nice things
% like protruding punctuation over the edge of the right margin to make
% the margin appear smoother. Basically it makes your book look more
% professional with very little effort. It also has a ton of options if
% you want to micromanage even more.
\usepackage{microtype}

% Disable headers and footer texts
\pagestyle{myheadings}

\def\DBKpublisher{Petter Reinholdtsen \\ Oslo}

% Cludge to replace DBKcover \def in
% /usr/share/texmf/tex/latex/dblatex/style/dbk_title.sty where author
% and publisher is missing
\def\DBKcover{
\ifthenelse{\equal{\DBKedition}{}}{\def\edhead{}}{\def\edhead{Ed. \DBKedition}}
% interligne double
\setlength{\oldbaselineskip}{\baselineskip}
\setlength{\baselineskip}{2\oldbaselineskip}
\textsf{
\vfill
\vspace{2.5cm}
\begin{center}
  \huge{\textbf{\DBKtitle}}\\ %
  \ifx\DBKsubtitle\relax\else%
    \underline{\ \ \ \ \ \ \ \ \ \ \ }\\ %
    \ \\ %
    \huge{\textbf{\DBKsubtitle}}\\ %
    \fi
  \  \\ %
  \huge{\DBKauthor} \\%
\end{center}
\vfill
\setlength{\baselineskip}{\oldbaselineskip}
\hspace{1cm}
\vspace{1cm}
\begin{center}
\Large{\DBKpublisher} \\
\end{center}
}

% Format for the other pages
\newpage
\setlength{\baselineskip}{\oldbaselineskip}
\lfoot[]{}
}

\begin{document}
% end of latex.begindocument
    </xsl:text>
  </xsl:param>
    <xsl:param name="local.l10n.xml" select="document('')"/>
  <l:i18n xmlns:l="http://docbook.sourceforge.net/xmlns/l10n/1.0">
    <l:l10n language="nb">
      <!-- Fix bugs in default nb locale -->
      <l:dingbat key="startquote" text="«"/>
      <l:dingbat key="endquote" text="»"/>
      <l:dingbat key="nestedstartquote" text="‘"/>
      <l:dingbat key="nestedendquote" text="’"/>
      <l:gentext key="Copyright" text=""/>
      <l:gentext key="copyright" text=""/>
    </l:l10n>
  </l:i18n>

</xsl:stylesheet>
