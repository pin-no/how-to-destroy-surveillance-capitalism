SOURCE=how-to-destroy-surveillance-capitalism
GENERATED = $(SOURCE).xml $(SOURCE).pdf $(SOURCE).html $(SOURCE).epub \
	$(SOURCE).nb.xml $(SOURCE).nb.pdf $(SOURCE).nb.html $(SOURCE).nb.epub

PANDOC_OPTS = \
  -t docbook4

DBLATEX_OPTS = \
  -p pdf.xsl

all: $(GENERATED)

# Workaround for missing titles
$(SOURCE).xml: $(SOURCE)-body.xml Makefile book.xml bookinfo.xml
	xmllint --nonet  --xinclude --postvalid book.xml > $@.new && \
		mv $@.new $@

$(SOURCE).pdf: $(SOURCE).xml Makefile pdf.xsl
	dblatex $(DBLATEX_OPTS) $(SOURCE).xml -o $@

$(SOURCE)-body.xml: $(SOURCE)-body.rst Makefile
	pandoc -s -o $@ $(PANDOC_OPTS) $(SOURCE)-body.rst
	sed -i \
		-e 's%“%<quote>%g' \
		-e 's%”%</quote>%g' $@

po/$(SOURCE).pot: $(SOURCE).xml
	po4a-gettextize -f docbook -m $(SOURCE).xml \
	-M UTF-8 -L UTF-8 \
	--package-name "How to Destroy Surveillance Capitalism" \
	--copyright-holder "Cory Doctorow" \
	--package-version "n/a" \
	| sed 's/CHARSET/UTF-8/' > $@.new && mv $@.new $@
po/$(SOURCE).%.po: po/$(SOURCE).pot
	po4a --no-translations --msgmerge-opt --no-location po4a.cfg

$(SOURCE).nb.xml: po/$(SOURCE).nb.po $(SOURCE).xml
	po4a --translate-only $(SOURCE).nb.xml po4a.cfg
$(SOURCE).nb.pdf: $(SOURCE).nb.xml Makefile pdf.xsl
	dblatex $(DBLATEX_OPTS) $(SOURCE).nb.xml -o $@

.xml.epub:
	dbtoepub -s epub.xsl $^ -o $@

.xml.html:
	xmlto -x docbook-utf8.xsl -m pdf.xsl html-nochunks $^

XMLLINTOPTS = --nonet --noout  --xinclude --postvalid 
lint: book.xml
	xmllint $(XMLLINTOPTS) $^

clean:
	$(RM) *~ $(GENERATED) $(SOURCE).xml

distclean: clean

stats:
	for f in po/*.po; do printf "$$f "; msgfmt --output /dev/null --statistics $$f; done

.SUFFIXES: .xml .html .pdf .epub
